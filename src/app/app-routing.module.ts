import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeasurementsComponent } from './pages/measurements/measurements.component';
import { EditMeasurementComponent } from './pages/edit-measurement/edit-measurement.component';


export const routes: Routes = [
  { path: 'measurements', component: MeasurementsComponent },
  { path: 'measurements/:id', component: EditMeasurementComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
