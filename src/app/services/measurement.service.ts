import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { MeasurementsComponent } from '../pages/measurements/measurements.component';

@Injectable({
  providedIn: 'root'
})
export class MeasurementService {
  link = 'http://localhost:60484/api/measurement';
  tmp: Measurement = { id: 1, name: "test", value: 12, createdBy: "Me", createdAt: new Date() }
  constructor(private client: HttpClient) { }
  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };
  getAllMeasurements = (): Observable<Measurement[]> =>
   this.client.get<Measurement[]>(this.link);
 
  getOneMeasurement(id: number): Observable<Measurement> {
    const url = `${this.link}/${id}`;
    return this.client.get<Measurement>(url, this.options);
  }
  deleteMeasurement(m_id: number) {
    const url = `${this.link}/${m_id}`;
    return this.client.delete(url, this.options);
  }
  addMeasurement = (measurement): Observable<Measurement> =>
    this.client.post<Measurement>(this.link, measurement, this.options);

  editMeasurement(id: number, measurement: Measurement): Observable<Measurement> {
    const url = `${this.link}/${id}`;
    return this.client.put<Measurement>(url, measurement, this.options);
  }
}
