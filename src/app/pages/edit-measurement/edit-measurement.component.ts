import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MeasurementService } from 'src/app/services/measurement.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-measurement',
  templateUrl: './edit-measurement.component.html',
  styleUrls: ['./edit-measurement.component.css']
})
export class EditMeasurementComponent implements OnInit {

  userId: number;
  measurement: Measurement;
  constructor(private router: Router, private acrivatedRoute: ActivatedRoute, private service: MeasurementService) {
    this.getUser();
  }

  measurementForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    value: new FormControl('', [Validators.required]),
    createdBy: new FormControl('', [Validators.required]),
    createdAt: new FormControl('', [Validators.required])
  });

  ngOnInit() {
  }
  getUser(): void {
    this.userId = +this.acrivatedRoute.snapshot.paramMap.get('id');
    this.service.getOneMeasurement(this.userId).subscribe(m => this.measurement = m, err => this.router.navigateByUrl('measurements'));
  }

  onFormSubmit() {
    this.service.editMeasurement(this.userId, this.measurementForm.value).subscribe(() => this.router.navigateByUrl('measurements'));
    console.log(this.measurementForm.value)
    this.measurementForm.reset();
  }


}
