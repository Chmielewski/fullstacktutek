import { Component, OnInit } from '@angular/core';
import { MeasurementService } from 'src/app/services/measurement.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AddMeasurementDialogComponent } from 'src/app/dialogs/add-measurement-dialog/add-measurement-dialog.component';

@Component({
  selector: 'app-measurements',
  templateUrl: './measurements.component.html',
  styleUrls: ['./measurements.component.css']
})
export class MeasurementsComponent implements OnInit {
  public measurements: Measurement[];
  constructor(private service: MeasurementService,public dialog: MatDialog ,private router: Router) { }

  ngOnInit() {
    this.service.getAllMeasurements().subscribe(data => {
      this.measurements = data;
    }, err => {
      console.log(err);
      window.alert("something went wrong");
    })
  }

  onEdit(id: number) {
    this.router.navigate(['/measurements', id]);
  }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(AddMeasurementDialogComponent, {
      width: '250px',
      data: { id: undefined,
        name: "",
        value: 0,
        createdBy: "",
        createdAt: new Date()}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.service.addMeasurement(result).subscribe(s => {
        result.id = s.id
        this.measurements.push(result)
      })
    });
  }

  deleteMeasurement(id: number) {
    this.service.deleteMeasurement(id).subscribe(s => {
      this.measurements = this.measurements.filter(x => x.id !== id);
    })
  }

}
