import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MeasurementsComponent } from './pages/measurements/measurements.component';
import { RouterModule } from '@angular/router';
import { routes } from '../app/app-routing.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { EditMeasurementComponent } from './pages/edit-measurement/edit-measurement.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddMeasurementDialogComponent } from './dialogs/add-measurement-dialog/add-measurement-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    MeasurementsComponent,
    EditMeasurementComponent,
    AddMeasurementDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    ReactiveFormsModule,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent,AddMeasurementDialogComponent]
})
export class AppModule { }
