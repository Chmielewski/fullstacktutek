import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-measurement-dialog',
  templateUrl: './add-measurement-dialog.component.html',
  styleUrls: ['./add-measurement-dialog.component.css']
})
export class AddMeasurementDialogComponent {

  constructor(public dialogRef: MatDialogRef<AddMeasurementDialogComponent>) {}

    data:Measurement = { id: undefined,
      name: undefined,
      value: undefined,
      createdBy: undefined,
      createdAt: undefined};
    measurementForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      value: new FormControl('', [Validators.required]),
      createdBy: new FormControl('', [Validators.required]),
      createdAt: new FormControl('', [Validators.required])
    });

  onNoClick(): void {
    this.dialogRef.close();
  }

  onFormSubmit():void{
    this.data.value = this.measurementForm.get('value').value;
    this.data.name = this.measurementForm.get('name').value;
    this.data.createdBy = this.measurementForm.get('createdBy').value;
    this.data.createdAt = this.measurementForm.get('createdAt').value; 
    this.dialogRef.close(this.data);
  }

}
