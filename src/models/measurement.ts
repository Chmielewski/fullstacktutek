interface Measurement {
    id: number,
    name: string,
    value: number,
    createdBy: string,
    createdAt: Date
}